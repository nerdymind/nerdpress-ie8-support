<?php
/**
 * Plugin Name: NerdPress IE 8 Support
 * Plugin URI: http://nerdymind.com/
 * Description: Adds support for Internet Explorer 8. Sigh.
 * Author: NerdyMind Marketing
 * Author URI: http://nerdymind.com/
 * Version: 1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

if ( ! class_exists( 'NerdPressIE8' ) ) {
	
class NerdPressIE8 {
	
	function __construct() {
		add_action( 'wp_head', array( &$this, 'add_libraries' ) );
	}
	
	function add_libraries() {
		echo '
		<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		';
	}
	
}

new NerdPressIE8();
	
}